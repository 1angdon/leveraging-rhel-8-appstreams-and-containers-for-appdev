[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)
# Lab 3: Using Our Application Streams
## Introduction

In this lab we will deploy a containerized database and create front-end containers for our Flask Application Streams.

This lab should be performed on `workstation.example.com` as `student` unless otherwise instructed.

Expected completion: 15-20 minutes

NOTE: much of the superflous output from the commands run here has been redacted for brevity. Please don't consider it an error.

## Prerequisites
This lab requires that you have completed [Lab 2](../lab2/chapter2.md).

If you have not done so (or have been unable to complete it successfully, there is an ansible playbook on `workstation.example.com` that will set the system up.

```
$ cd ~/appstream-lab/playbooks
$ ansible-playbook -i hosts --tags=lab3 lab-setup-play.yml
```
This may take up to 20 minutes to complete, depending on the current system state.

## Getting a database

Red Hat provides a significant number of containerized applications in the [Red Hat Container Catalog](https://access.redhat.com/containers/) from both Red Hat and Third Parties.
You should definitely consider this resource for high quality, trustable, easy to access packaged applications.
For this lab, we will be leveraging one of the PostgreSQL containers as the backend for our ACME software.

Browse to the [Red Hat Container Catalog](https://access.redhat.com/containers/).

Enter "postgresql" in the search bar.
Select ["rhscl/postgresql-96-rhel7"](https://access.redhat.com/containers/?tab=overview#/registry.access.redhat.com/rhscl/postgresql-96-rhel7) repository.
Select "Get This Image" tab.
Scroll down to "Using podman login" section.

However, for the sake of the internet performance at a conference, we are actually going to pull the container from our local repository.

Need to pull and run as root since podman doesn't yet support rootless port binding.

```bash
$ sudo podman pull core.example.com:5000/postgresql-96-rhel7
$ sudo podman run -d \
    --name postgresql_database \
    -e POSTGRESQL_USER=user \
    -e POSTGRESQL_PASSWORD=pass \
    -e POSTGRESQL_DATABASE=db \
    --net=host \
    core.example.com:5000/postgresql-96-rhel7
```

As an interesting aside, if you notice, this is a PostgreSQL database running based on a RHEL7 container.
As you may or may not know, you can, generally, mix and match containers across RHEL versions.
For specific scenarios, you should consult the [Customer Portal](https://access.redhat.com).

In order to verify the database is working, let's connect to it.
However, the PostgreSQL client isn't installed by default.
So, let's go get it.
We are going to use the PostgreSQL module and the ```client``` profile.

```bash
$ sudo yum install -y @postgresql/client
```

If you followed along in [Lab 1](../lab1/chapter1.md), you may recall this was already installed and you'll see a "Nothing to do." message.
Otherwise, you should see messages about installing the ```postgresql``` rpm, the ```postgresql/client``` profile, and enabling the ```postgresql:10``` module stream.

Now we can connect and make sure the database is really running.
The password is "pass"
```
$ psql -h localhost -p 5432 -d db -U user
db=>
db=>\dt
Did not find any relations.
db=>\q
$
```

## Create Front-End Container Python27

For the sake of simplicity we have created a very simple Python Flask application in the labs/lab3/app directory.
Now we will create an OCI file in which to run our application.
However, we want to base our container on RHEL8.
In order to do this, we can leverage the new "Universal Base Image" (ubi) which is available for both RHEL7 & RHEL8.
To access the ubi images you can find them in the Red Hat Container Catalog here:

* registry.access.redhat.com/ubi7/ubi
* registry.access.redhat.com/ubi8/ubi

However, as before, we are going to use a local copy for perfomance reasons.

```bash
$ sudo podman pull core.example.com:5000/ubi8/ubi
```

First, we are going to create an image we can build on that has all the repositories available.


```bash
$ mkdir -p ~/flasklab/frontend
$ cd ~/flasklab/frontend
$ cp ~/appstream-lab/labs/lab3/app/app.py .
$ cp -a ~/appstream-lab/labs/lab3/app/templates .
$ cp ~/appstream-lab/labs/lab3/app/local.repo .
$ cp ~/appstream-lab/labs/lab3/app/rhel8.repo .
```

Here is our OCIfile.

```docker
$ cat <<EOF > base.OCIfile
FROM core.example.com:5000/ubi8/ubi

ADD ../repos/py27 /opt/flask27_module
ADD ../repos/py36 /opt/flask36_module
RUN rm /etc/yum.repos.d/*
ADD local.repo /etc/yum.repos.d/local.repo
ADD rhel8.repo /etc/yum.repos.d/rhel8.repo

EOF
```
Now we build the base image, just for simplicity.
Be careful not to miss the "`.`" at the end of the line that instructs podman what directory to build from.

```bash
$ sudo podman build -t flask-base --file=base.OCIfile .
```

Next we have to create our application image.
Here is our OCIfile.

```docker
$ cat <<EOF > flask27.OCIfile
FROM flask-base

RUN yum \
    module -y enable flask:py27 \
    && yum clean all

RUN yum \
    install -y python2 python2-flask \
    python2-flask-sqlalchemy python2-psycopg2 \
    && yum clean all

#copy our app in to the container
ADD app.py /app/app.py
ADD templates /app/templates
EXPOSE 5000

CMD [ "/usr/bin/python2", "/app/app.py" ]

EOF
```

Now we build and run the application using podman.
Note: We need to build and run as root since podman doesn't yet support rootless port binding.

```bash
$ sudo podman build -t flask27 --file=flask27.OCIfile .
$ sudo podman run --rm \
    -p 5000:5000 \
    -e DBUSER="user" \
    -e DBPASS="pass" \
    -e DBHOST="workstation.example.com" \
    -e DBNAME="db" \
    flask27
```

[//]: # (#TODO confirm the hostname construct)

You can now open a web browser on your computer and connect to the new application using the hostname that you have been ssh'ing to the machine with.
The name should be something like `appstream-2f1c.rhpds.opentlc.com` and URL would then be `http://appstream-2f1c.rhpds.opentlc.com:5000`

Go ahead and play around with the application a bit.
When you are done you can shut down the server by hitting `Ctrl+C`.

## Create Front-End Container Python36

Ok, as we said at the very beginning, we really want this application to be running in Python 3.
However, we don't trust that Python 3 will be deployed in the Data Center in time for the launch of our Acme Corp Blog app.
Now that we have a working Python 2 version, let's spin up a Python 3 version.

For that we will need a new OCIfile listing the Python 3 dependencies.

```docker
$ cat <<EOF > flask36.OCIfile
FROM flask-base

RUN yum \
    module -y enable flask:py36 \
    && yum clean all

RUN yum \
    install -y python3 python3-flask \
    python3-flask-sqlalchemy python3-psycopg2 \
    && yum clean all

#copy our app in to the container
ADD app.py /app/app.py
ADD templates /app/templates
EXPOSE 5000

CMD [ "/usr/bin/python3", "/app/app.py" ]

EOF
```
Now let's build this new container and run it against the existing database.

```bash
$ sudo podman build -t flask36 --file=flask36.OCIfile .
$ sudo podman run --rm \
    -p 5000:5000 \
    -e DBUSER="user" \
    -e DBPASS="pass" \
    -e DBHOST="workstation.example.com" \
    -e DBNAME="db" \
    flask36
```

You can now open a web browser on your computer and connect to the Python 3 version of the application with the same URL used earlier for the Python 2 version.
Go ahead and play around with the application again.
Since it's using the same database, anything you entered earlier will still be there.
When you are done you can shut down the server by hitting `Ctrl+C`.

#TODO needs some closing remarks
