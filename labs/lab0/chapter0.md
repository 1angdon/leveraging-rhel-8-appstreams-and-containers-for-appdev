[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)
# Lab 0: Lab Setup & Basic Commands
## Introduction

In this lab we will get you connected and show you the basics of working in the lab.

Expected completion: 5-10 minutes

## Find your VM Instance
This lab is designed to accommodate many students.
As a result, each student will be given a set of VMs.
You will have two:

* workstation.example.com
* core.example.com

The `core` machine will not be used directly but is the cache for all the RPMs, modules, and containers we will be using in this lab.
Everything you do will be on the `workstation` machine.

In order to access the `workstation` machine, you should see a web browser open on your computer.
Please keep this window open during the entire lab.

## Connecting to your VM
Open a terminal and copy & paste the ssh line you should see in the web page.
The password is also displayed there.

## Getting Set Up
For the sake of time, some of the required setup has already been taken care of on your VM.

Since some of these labs will have long running processes, it is recommended to use `tmux` in case you lose your connection at some point so you can reconnect:
```bash
$ tmux
```

In case you get disconnected use `tmux attach` to reattach once you reestablish ssh connectivity. If you are unfamiliar with tmux here is a [quick tutorial](https://fedoramagazine.org/use-tmux-more-powerful-terminal/).

## Lab Materials

Refresh the lab repository from gitlab:
```bash
$ cd ~/appstream-lab
$ git pull https://gitlab.com/redhatsummitlabs/leveraging-rhel-8-appstreams-and-containers-for-appdev.git
```
Note: you can find all the other labs from Red Hat Summit 2019 in the same [GitLab "Group."](https://gitlab.com/redhatsummitlabs)

You are now ready to move on to the [next lab](../lab1/chapter1.md).
